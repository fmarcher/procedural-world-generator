// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Chunk.h"
#include "CoreMinimal.h"
#include "ProceduralMeshComponent.h"
#include "GameFramework/Actor.h"

#include "Generator.generated.h"

struct MeshData
{
	TArray<FVector>* vertices;
	TArray<int32>* triangles;
	TArray<FVector>* normals;
	TArray<FVector2D>* UV;
	TArray<FColor>* VertexColors;
	TArray<FProcMeshTangent>* tangents;

	MeshData(int count)
	{
		vertices = new TArray<FVector>();
		triangles = new TArray<int32>();
		normals = new TArray<FVector>();
		UV = new TArray<FVector2D>();
		VertexColors = new TArray<FColor>();
		tangents = new TArray<FProcMeshTangent>();

		vertices->Reserve(count * count);
		normals->Reserve(count * count);
		UV->Reserve(count * count);
		VertexColors->Reserve(count * count);
		triangles->Reserve((count - 1) * (count - 1) * 2);
	}

	~MeshData()
	{
		delete vertices;
		delete triangles;
		delete normals;
		delete UV;
	}
};

USTRUCT(BlueprintType)
struct FLod
{
	GENERATED_BODY()
    float distance;
    int size;
};

USTRUCT(BlueprintType)
struct FChunkData
{
	GENERATED_BODY()
    FIntPoint index;
	int size;
	struct MeshData* mesh_data;
	class AChunk* chunk;
	FChunkData* neighbors[4];
	int border_lod[4];
};



class SimplexNoise;
class AChunk;
class AproceduralWorldCharacter;

UCLASS()
class PROCEDURALWORLD_API AGenerator : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGenerator();
	~AGenerator();
		
	virtual void Tick( float DeltaSeconds );

	int getLOD(float distance) const;
	static int getLOD(TMap<float, int> LOD, float distance);

	class AChunk* FindChunk(const FIntPoint& Index);
	FChunkData* FindChunkData(const FIntPoint& Index);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
    FVector scale;
	UPROPERTY(BlueprintReadOnly)
    float render_distance;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TMap<float, int> LOD;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Noise")
	class UNoiseData* noise_data;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	AproceduralWorldCharacter* player;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int time_per_tick = 50;

	// UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	TArray<FChunkData*> chunks;

	SimplexNoise* noise;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	FIntPoint player_position_index = FIntPoint(6969420, 6969420);

	class QuadTree* quad_tree;

	UPROPERTY(EditAnywhere)
	TArray<FString> tags_to_ignore;
	
	UFUNCTION(BlueprintImplementableEvent)
    void CalculateTangentsForMesh(const TArray<FVector>& vertices, const TArray<int32>& indices, const TArray<FVector2D>& uv, TArray<FVector>& normals, TArray<FProcMeshTangent>& tangents);

	UFUNCTION(BlueprintCallable)
	float getLevelNoise(FVector2D p) const;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// virtual void BeginDestroy() override;

	void generateChunk(struct FChunkData* element);
	UFUNCTION(BlueprintCallable, Category = "My Functions")
	void Init();
	UFUNCTION(BlueprintImplementableEvent)
	void SpawnChunk(FVector scale_, AChunk*& chunk);
	
	class GeneratorBackground* background_generator_chunk;
	FRunnableThread* background_thread_chunk;

};
