// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"


UENUM(BlueprintType)
enum EDirections
{
	Left,
    Right,
    Up,
    Down
};

/**
 * 
 */
class PROCEDURALWORLD_API Global
{
public:
	Global();
	~Global();

	static FIntPoint directions[];
	static FString FDirections[];

	static class AGenerator* generator;

	static EDirections ReverseDirection(EDirections direction)
	{
		return ReverseDirection(static_cast<int>(direction));
	}
	static EDirections ReverseDirection(int direction)
	{
		return static_cast<EDirections>(direction % 2 == 0 ? direction + 1 : direction - 1);
	}

};