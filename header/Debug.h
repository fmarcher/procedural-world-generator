// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
class PROCEDURALWORLD_API Debug
{
public:
	Debug();
	~Debug();

	static void Log(FString tag, FString message, UObject* context = nullptr, bool to_screen = false);
	static void Error(FString tag, FString message, UObject* context = nullptr, bool to_screen = false);

	static FString Int(int i);
	static FString Int(void* i);
	static FString Float(float f);
};
