// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"



#include "Chunk.generated.h"

struct FChunkData;
enum EDirections;
class UProceduralMeshComponent;
class SimplexNoise;
struct MeshData;

UCLASS()
class PROCEDURALWORLD_API AChunk : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AChunk();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
        UProceduralMeshComponent* mesh;

	
    
    MeshData* GenerateMeshData() const;
	static void CheckBorder(FChunkData* data, EDirections direction, int (getIndex1)(int diff, int j, int size, int count), int (getIndex2)(int diff, int j, int size, int count), int (getVertex)(int i, int size, int count));

	SimplexNoise* noise;
	FChunkData* data;

public:

	void Init(SimplexNoise* noise_, FChunkData* data_);


	static MeshData* GenerateMeshData(FIntPoint index, int size, FVector scale, SimplexNoise* noise);

    void GenerateMesh(MeshData& mesh_data) const;

	static void CheckBorderLeft(FChunkData* data);
	static void CheckBorderRight(FChunkData* data);
	static void CheckBorderUp(FChunkData* data);
	static void CheckBorderDown(FChunkData* data);

	FIntPoint GetIndex() const;
	int GetLOD() const;
	AChunk* GetNeighbor(EDirections direction) const;
	AChunk* GetNeighbor(int direction) const;
	void InvalidateNeighbor(int index) const;
	FChunkData* GetData() const;

	void SetNeighbors(FChunkData* neighbor, EDirections direction) const;

	UFUNCTION(BlueprintCallable)
	FString GetDataText() const;

};
