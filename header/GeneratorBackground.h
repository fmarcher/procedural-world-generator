// Fill out your copyright notice in the Description page of Project Settings.

#pragma once


#include <mutex>

#include "CoreMinimal.h"

struct Task
{
	void(*task)(void*);
	void* parameter;
};

class PROCEDURALWORLD_API GeneratorBackground : public FRunnable
{

private:
	static std::queue<Task> task_queue;
	
	static std::queue<struct FChunkData*> queue;
	static std::mutex lock;

	const int COUNT = std::thread::hardware_concurrency() - 1;

	static void Generate(FChunkData* element);
	static void InitChunkData(FChunkData* data);

	static class AGenerator* generator;
	
	static bool BorderNeedsUpdate(FChunkData* data, enum EDirections direction, int size);
	
public:
	GeneratorBackground(AGenerator* generator_, long sleep_duration_);
	~GeneratorBackground();

	// FRunnable functions
	virtual uint32 Run() override;
	virtual void Stop() override {running = false;}
	virtual void Exit() override {running = false;}
	
	void CheckChunks();
	static void DeleteChunk(class AChunk* Chunk);
	static void CheckBorder(FChunkData* chunk, bool own_only = false);
	static void UpdateLOD(int size, AChunk* chunk, bool force = false);

	static void Enqueue(FChunkData* element);
	static bool TryTake(FChunkData*& element);

	static void EnqueueTask(Task task);
	static void CompleteTasks();

	long sleep_duration = 500;
	bool running = true;
};

//*/