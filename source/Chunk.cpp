// Fill out your copyright notice in the Description page of Project Settings.


#include "../header/Chunk.h"

#include "../header/Generator.h"
#include "../header/GeneratorBackground.h"
#include "ProceduralMeshComponent.h"
#include "Kismet/KismetSystemLibrary.h"
#include "../header/Debug.h"
#include "../header/Global.h"
#include "../../SimplexNoise/SimplexNoise.h"

// Sets default values
AChunk::AChunk()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	mesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("TerrainMesh"));
	RootComponent = mesh;
	mesh->bUseAsyncCooking = true;

}

// Called when the game starts or when spawned
void AChunk::BeginPlay()
{
	Super::BeginPlay();
	
}


void AChunk::Init(SimplexNoise* noise_, FChunkData* data_)
{
	noise = noise_;
	data = data_;

	data->chunk = this;
}


MeshData* AChunk::GenerateMeshData() const
{
	return GenerateMeshData(data->index, data->size, Global::generator->scale, noise);
}

MeshData* AChunk::GenerateMeshData(FIntPoint index, int size, FVector scale, SimplexNoise* noise)
{
	const auto mesh_data = new MeshData((size + 1));

	TArray<FVector> normal_helper;

	for(int x = -1; x <= size; x++)
	{
		for(int y = -1; y <= size; y++)
		{
			auto Pos = FVector2D(x * scale.X / size + index.X * scale.X, y * scale.Y / size + index.Y * scale.Y);

			const auto n = noise->GetLevelNoise(Pos / 100);

			if(x == -1 || y == -1)
			{
				normal_helper.Add(FVector{static_cast<float>(x) / size, static_cast<float>(y) / size, n.X + n.Y} * scale);
				continue;
			}

			mesh_data->VertexColors->Add({uint8(n.Y * 5),0, 0, 0});
			mesh_data->vertices->Add(FVector{static_cast<float>(x) / size, static_cast<float>(y) / size, n.X + n.Y} * scale);
			mesh_data->UV->Add(FVector2D{static_cast<float>(x) / size, static_cast<float>(y) / size});

			if(x == 0 || y == 0)
				continue;

			mesh_data->triangles->Append({
                mesh_data->vertices->Num() - 1, mesh_data->vertices->Num() - 2, mesh_data->vertices->Num() - 2 - size,
                mesh_data->vertices->Num() - 2 - size, mesh_data->vertices->Num() - 2, mesh_data->vertices->Num() - 3 - size
                });
		}
	}

	for(int i = 0; i < mesh_data->vertices->Num(); i++)
	{
		FVector v1 = (*mesh_data->vertices)[i];
		FVector v2;
		FVector v3;

		if(i % (size + 1) == 0)
			v2 = normal_helper[size + 2 + i / (size + 1)];
		else
			v2 = (*mesh_data->vertices)[i - 1];
		
		if(i <= size)
			v3 = normal_helper[i + 1];
		else
			v3 = (*mesh_data->vertices)[i - size - 1];

		auto normal = FVector::CrossProduct(v3 - v1, v2 - v1);
		normal.Normalize();

		// Debug::Log("Normals", "calculate: index: " + Debug::Int(i) + ", " + Debug::Int(index_2) + ", " + Debug::Int(index_3) + ", v1: " + v1.ToString() + ", v2: " + v2.ToString() + ", v3: " + v3.ToString() + ", normal: " + normal.ToString() + ", v2 - v1: " + (v2 - v1).ToString() + ", v3 - v1: " + (v3 - v1).ToString());
		
		//ABxAC -> (B-A)x(C-A)
		mesh_data->normals->Add(normal);
	}

	return mesh_data;
}

void AChunk::GenerateMesh(MeshData& mesh_data) const
{
	mesh->CreateMeshSection(0, *mesh_data.vertices, *mesh_data.triangles, *mesh_data.normals, *mesh_data.UV, *mesh_data.VertexColors,{}, true);
}

void AChunk::CheckBorder(FChunkData* data, EDirections direction, int(getIndex1)(int diff, int j, int size, int count),
	int(getIndex2)(int diff, int j, int size, int count), int(getVertex)(int i, int size, int count))
{
	if(data->index == Global::generator->player_position_index || true)
		Debug::Log("Chunk", "start updating Border, index: " + data->index.ToString() + " direction: " + Global::FDirections[direction] +
            " size: " + Debug::Int(data->size) + " neighbor: " + Debug::Int(data->neighbors[direction]));

	const float distance = FVector2D(data->index - Global::generator->player_position_index + Global::directions[direction]).Size();
	const int neighbor_size = Global::generator->getLOD(distance);
	const int diff = float(data->size) / neighbor_size;
	const int count = data->mesh_data->vertices->Num() - 1;

	if(data->index == Global::generator->player_position_index || true)
		Debug::Log("Chunk", "Updating border, index: " + data->index.ToString() + "player pos: " + Global::generator->player_position_index.ToString() + " direction: " + Global::FDirections[direction] + " distance: " + Debug::Float(distance) +
		" size: " + Debug::Int(data->size) + " neighbor size: " + Debug::Int(neighbor_size) + " diff: " + Debug::Int(diff) + " count: " + Debug::Int(count) + " borderLOD: " + Debug::Int(data->border_lod[direction]));
	
	if(diff <= 1)
	{
		if(data->border_lod[direction] != data->size)
		{
			GeneratorBackground::UpdateLOD(data->size, data->chunk);
		}
		return;
	}
	
	for (int i = 1; i < data->size; i++)
	{			
		const int j = i / float(diff);
		const int rel_i = i - j * diff;
		
		if(rel_i == 0)
			continue;

		const int index1 = getIndex1(diff, j, data->size, count);
		const int index2 = getIndex2(diff, j, data->size, count);
		const int vertex = getVertex(i, data->size, count);

		const float val = (*data->mesh_data->vertices)[index1].Z * (1 - rel_i / float(diff)) +
			(*data->mesh_data->vertices)[index2].Z * (rel_i / float(diff));
		(*data->mesh_data->vertices)[vertex].Z = val;// + 100;
	}
	
	data->border_lod[direction] = neighbor_size;
	Debug::Log("Chunk", "updated border of " + data->index.ToString() + " " + Global::FDirections[direction] + " neighbor size: " + Debug::Int(neighbor_size) + " size: " + Debug::Int(data->size));
}

void AChunk::CheckBorderLeft(FChunkData* data)
{
	auto getIndex1 = [](int diff, int j, int size, int count){ return j * diff; };//up
	auto getIndex2 = [](int diff, int j, int size, int count){ return (j + 1) * diff; };
	auto getVertex = [](int i, int size, int count){ return i; };

	CheckBorder(data, Left, getIndex1, getIndex2, getVertex);
}

void AChunk::CheckBorderRight(FChunkData* data)
{
	auto getIndex1 = [](int diff, int j, int size, int count){ return count - j * diff; };//left
	auto getIndex2 = [](int diff, int j, int size, int count){ return count - (j + 1) * diff; };
	auto getVertex = [](int i, int size, int count){ return count - i; };

	CheckBorder(data, Right, getIndex1, getIndex2, getVertex);
}

void AChunk::CheckBorderUp(FChunkData* data)
{
	auto getIndex1 = [](int diff, int j, int size, int count){ return j * diff * (size + 1) + size; }; //right
	auto getIndex2 = [](int diff, int j, int size, int count){ return (j + 1) * diff * (size + 1) + size; };
	auto getVertex = [](int i, int size, int count){ return i * (size + 1) + size; };

	CheckBorder(data, Up, getIndex1, getIndex2, getVertex);
}

void AChunk::CheckBorderDown(FChunkData* data)
{
	auto getIndex1 = [](int diff, int j, int size, int count){ return j * diff * (size + 1); };//down
	auto getIndex2 = [](int diff, int j, int size, int count){ return (j + 1) * diff * (size + 1); };
	auto getVertex = [](int i, int size, int count){ return i * (size + 1); };

	CheckBorder(data, Down, getIndex1, getIndex2, getVertex);
}

FIntPoint AChunk::GetIndex() const
{
	return data->index;
}

int AChunk::GetLOD() const
{
	return data->size;
}

AChunk* AChunk::GetNeighbor(EDirections direction) const
{
	return data->neighbors[direction] ? data->neighbors[direction]->chunk : nullptr;
}

AChunk* AChunk::GetNeighbor(int direction) const
{
	return data->neighbors[direction] ? data->neighbors[direction]->chunk : nullptr;
}

void AChunk::InvalidateNeighbor(int index) const
{
	data->neighbors[index] = nullptr;
}

FChunkData* AChunk::GetData() const
{
	return data;
}

void AChunk::SetNeighbors(FChunkData* neighbor, EDirections direction) const
{
	data->neighbors[direction] = neighbor;
}

FString AChunk::GetDataText() const
{
    if(!data)
		return "null";
	
	return FString::Printf(TEXT("index: %s\nsize %d\ndata %p\nchunk %p\nneighbor left %p %s\nneighbor right %p %s\nneighbor up %p %s\nneighbor down %p %s\nlod left %d\nlod right %d\nlod up %d\nlod down %d"),
	    *data->index.ToString(), data->size, data->mesh_data, data->chunk,
	    data->neighbors[Left],  data->neighbors[Left] ? *data->neighbors[Left]->index.ToString() : *FString(""),
	    data->neighbors[Right], data->neighbors[Right] ? *data->neighbors[Right]->index.ToString() : *FString(""),
	    data->neighbors[Up],    data->neighbors[Up] ? *data->neighbors[Up]->index.ToString() : *FString(""),
	    data->neighbors[Down],  data->neighbors[Down] ? *data->neighbors[Down]->index.ToString() : *FString(""),
	    data->border_lod[Left], data->border_lod[Right], data->border_lod[Up], data->border_lod[Down]);
}



