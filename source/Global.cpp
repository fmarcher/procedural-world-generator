// Fill out your copyright notice in the Description page of Project Settings.


#include "../header/Global.h"

FIntPoint Global::directions[] = {FIntPoint(-1, 0), FIntPoint(1, 0), FIntPoint(0, 1), FIntPoint(0, -1)};
FString Global::FDirections[] = {"Left", "Right", "Up", "Down"};
AGenerator* Global::generator;

Global::Global()
{
}

Global::~Global()
{
}
