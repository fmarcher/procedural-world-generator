// Fill out your copyright notice in the Description page of Project Settings.


#include "../header/Debug.h"



#include "../header/Global.h"
#include "../header/Generator.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetStringLibrary.h"
#include "Kismet/KismetSystemLibrary.h"

Debug::Debug()
{
}

Debug::~Debug()
{
}

void Debug::Log(FString tag, FString message, UObject* context, bool to_screen)
{
    if(Global::generator->tags_to_ignore.Contains(tag) || Global::generator->tags_to_ignore.Contains("all"))
        return;
    UKismetSystemLibrary::PrintString(context, "[" + tag + "] " + message, to_screen);
}

void Debug::Error(FString tag, FString message, UObject* context, bool to_screen)
{
    if(Global::generator->tags_to_ignore.Contains(tag) || Global::generator->tags_to_ignore.Contains("all"))
        return;
    UKismetSystemLibrary::PrintString(context, "[" + tag + "] " + message, to_screen, true, FLinearColor::Red);
}

FString Debug::Int(int i)
{
    return FString::FromInt(i);
}

FString Debug::Int(void* i)
{
    return FString::Printf(TEXT("%p"), i);
}

FString Debug::Float(float f)
{
    return UKismetStringLibrary::Conv_FloatToString(f);
}
