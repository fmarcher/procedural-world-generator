// Fill out your copyright notice in the Description page of Project Settings.


#include "../header/GeneratorBackground.h"

#include <thread>


#include "../header/Generator.h"
#include "../header/Chunk.h"
#include "Kismet/KismetMathLibrary.h"
#include "../header/Debug.h"
#include "../header/Global.h"
#include "proceduralWorld/proceduralWorldCharacter.h"

std::queue<FChunkData*> GeneratorBackground::queue;
std::queue<Task> GeneratorBackground::task_queue;
std::mutex GeneratorBackground::lock;
AGenerator* GeneratorBackground::generator;

void GeneratorBackground::Generate(FChunkData* element)
{
    element->mesh_data = AChunk::GenerateMeshData(element->index, element->size, generator->scale, generator->noise);
    for (int i = 0; i < 4; i++)
        element->border_lod[i] = element->size;


    Debug::Log("Background", "BG: Generated mesh " + element->index.ToString());

}

GeneratorBackground::GeneratorBackground(AGenerator* generator_, long sleep_duration_) : sleep_duration(sleep_duration_)
{
    generator = generator_;
}

GeneratorBackground::~GeneratorBackground()
{
}



void GeneratorBackground::Enqueue(FChunkData* element)
{
    lock.lock();
    queue.push(element);
    lock.unlock();
}

bool GeneratorBackground::TryTake(FChunkData*& element)
{
    if(queue.empty())
        return false;
    element = queue.front();
    queue.pop();
    return true;
}

void GeneratorBackground::EnqueueTask(Task task)
{
    lock.lock();
    task_queue.push(task);
    lock.unlock();
}

void GeneratorBackground::CompleteTasks()
{
    while(!task_queue.empty())
    {
        auto& task = task_queue.front();
        task_queue.pop();
        task.task(task.parameter);
        Debug::Log("Background", "Completed task " + Debug::Int(task.task));
    }
}

////////////////////////////////////////////////////


uint32 GeneratorBackground::Run()
{
    while (running)
    {
        CheckChunks();
        CompleteTasks();
        std::this_thread::sleep_for(std::chrono::milliseconds(sleep_duration));
    }
    return 0;
}


void GeneratorBackground::CheckChunks()
{
    if(!generator || !generator->player)
        return;
    
    const auto current_pos = FIntPoint( UKismetMathLibrary::Round(generator->player->GetActorLocation().X / generator->scale.X - 0.5),
        UKismetMathLibrary::Round(generator->player->GetActorLocation().Y / generator->scale.Y - 0.5));
    
    
    if(current_pos == generator->player_position_index && false)
        return;
    Debug::Log("Background", "Player moved from " + generator->player_position_index.ToString() + " to " + current_pos.ToString() + "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
    generator->player_position_index = current_pos;

    FIntPoint index;
    for (index.X = -generator->render_distance - 1; index.X <= generator->render_distance + 1; index.X++)
    {
        for (index.Y = -generator->render_distance - 1; index.Y <= generator->render_distance + 1; index.Y++)
        {
            AChunk* chunk = generator->FindChunk(index + generator->player_position_index);
            const float distance = FVector2D(index).Size();
            const int lod = generator->getLOD(distance);

            Debug::Log("Background", "Index: " + index.ToString() + " Check chunk " + (index + generator->player_position_index).ToString() + " chunk: " + Debug::Int(chunk) + " distance: " + Debug::Float(distance) + " lod: " + Debug::Int(lod));
            if(lod == -1)
            {
                if(chunk != nullptr)
                    DeleteChunk(chunk);
                continue;
            }

            if(chunk == nullptr)
            {
                const auto new_chunk = new FChunkData{index + generator->player_position_index, lod};
                Generate(new_chunk);
                InitChunkData(new_chunk);
                CheckBorder(new_chunk);
                Enqueue(new_chunk);
            }
            else if(chunk->GetLOD() != lod)
            {
                UpdateLOD(lod, chunk);
                CheckBorder(chunk->GetData());
                Enqueue(chunk->GetData());
            }
        }
    }
}

void GeneratorBackground::UpdateLOD(int size, AChunk* chunk, bool force)
{
    if(size == -1 || !force && size == chunk->GetLOD())
        return;
    Debug::Log("Background", "Updating lod of " + chunk->GetIndex().ToString() + " from " + FString::FromInt(chunk->GetLOD()) + " to " + FString::FromInt(size));
    chunk->GetData()->size = size;
    Generate(chunk->GetData());
}

void GeneratorBackground::DeleteChunk(AChunk* Chunk)
{
    for (int i = 0; i < 4; i++)
    {
        if(Chunk->GetNeighbor(i))
            Chunk->GetNeighbor(i)->InvalidateNeighbor(Global::ReverseDirection(i));
    }
    
    generator->chunks.Remove(Chunk->GetData());
    Chunk->GetData()->mesh_data = nullptr;
    Enqueue(Chunk->GetData());
    Debug::Log("Background", "Delete chunk " + Chunk->GetIndex().ToString());

}

void GeneratorBackground::CheckBorder(FChunkData* chunk, bool own_only)
{
    Debug::Log("Background", "Check Border of " + chunk->index.ToString());

    AChunk::CheckBorderRight(chunk);
    AChunk::CheckBorderLeft(chunk);
    AChunk::CheckBorderUp(chunk);
    AChunk::CheckBorderDown(chunk);

    if(own_only)
        return;
    
    if(chunk->neighbors[Right] && BorderNeedsUpdate(chunk->neighbors[Right], Left, chunk->size))
    {
        UpdateLOD(chunk->neighbors[Right]->size, chunk->neighbors[Right]->chunk, true);
        CheckBorder(chunk->neighbors[Right], true);
        Enqueue(chunk->neighbors[Right]);
    }
    if(chunk->neighbors[Left] && BorderNeedsUpdate(chunk->neighbors[Left], Right, chunk->size))
    {
        UpdateLOD(chunk->neighbors[Left]->size, chunk->neighbors[Left]->chunk, true);
        CheckBorder(chunk->neighbors[Left], true);
        Enqueue(chunk->neighbors[Left]);
    }
    if(chunk->neighbors[Up] && BorderNeedsUpdate(chunk->neighbors[Up], Down, chunk->size))
    {
        UpdateLOD(chunk->neighbors[Up]->size, chunk->neighbors[Up]->chunk, true);
        CheckBorder(chunk->neighbors[Up], true);
        Enqueue(chunk->neighbors[Up]);
    }
    if(chunk->neighbors[Down] && BorderNeedsUpdate(chunk->neighbors[Down], Up, chunk->size))
    {
        UpdateLOD(chunk->neighbors[Down]->size, chunk->neighbors[Down]->chunk, true);
        CheckBorder(chunk->neighbors[Down], true);
        Enqueue(chunk->neighbors[Down]);
    }
}

bool GeneratorBackground::BorderNeedsUpdate(FChunkData* data, EDirections direction, int size)
{
    const auto own_size = Global::generator->getLOD(FVector2D(data->index - Global::generator->player_position_index).Size());
    Debug::Log("Border", "Check if Update needed: " + Global::FDirections[direction] + " Border of " + data->index.ToString() + " borderLOD: " + Debug::Int(data->border_lod[direction]) + " neighbor size: " + Debug::Int(size) + " own size: " + Debug::Int(data->size) + " calculated own size: " + Debug::Int( own_size) + " chunk: " + Debug::Int(data->chunk));

    if(!data->chunk)
        return false;
    if(data->border_lod[direction] == size)
        return false;
    if(own_size != size && false)
        return false;

    Debug::Log("Border", "Updating " + Global::FDirections[direction] + " Border of " + data->index.ToString() + " borderLOD: " + Debug::Int(data->border_lod[direction]) + " neighbor size: " + Debug::Int(size) + " own size: " + Debug::Int(data->size) + " calculated own size: " + Debug::Int( own_size));
    return true;
}


void GeneratorBackground::InitChunkData(FChunkData* data)
{
    generator->chunks.Add(data);

    
    for(int i = 0; i < 4; i++)
    {
        const auto neighbor = generator->FindChunkData(data->index + Global::directions[i]);
        if(!neighbor)
            continue;
        data->neighbors[i] = neighbor;
        neighbor->neighbors[Global::ReverseDirection(i)] = data;
    }
}



//*/
