// Fill out your copyright notice in the Description page of Project Settings.


#include "../header/Generator.h"

#include <chrono>

#include "../header/Chunk.h"
#include "../header/GeneratorBackground.h"
#include "Kismet/KismetMathLibrary.h"
#include "../header/Debug.h"
#include "../header/Global.h"
#include "../../SimplexNoise/SimplexNoise.h"



// Sets default values
AGenerator::AGenerator()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Global::generator = this;
	
}

float AGenerator::getLevelNoise(FVector2D p) const
{
	return noise->GetLevelNoiseF(p);
}

// Called when the game starts or when spawned
void AGenerator::BeginPlay()
{
	Super::BeginPlay();

	for (auto && lod_element : LOD)
	{
		if(lod_element.Key > render_distance)
			render_distance = lod_element.Key;
	}
	UE_LOG(LogTemp, Display, TEXT("set render distance to %f"), render_distance);

	Init();
}

AGenerator::~AGenerator()
{
	if(background_generator_chunk)
		background_generator_chunk->Exit();
	if(background_thread_chunk)
		background_thread_chunk->Kill();
}


void AGenerator::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	auto start = std::chrono::high_resolution_clock::now();

	int i = 0;
	while (true)
	{
		FChunkData* element = nullptr;
		if(!GeneratorBackground::TryTake(element))
			break;	

		i++;

		if(!element->mesh_data)
		{
			element->chunk->Destroy();
		}
		else if(!element->chunk)
			generateChunk(element);
		else if(IsValid(element->chunk))
			element->chunk->GenerateMesh(*element->mesh_data);

		auto now = std::chrono::high_resolution_clock::now();
		if(std::chrono::duration<double, std::milli>(now - start).count() > time_per_tick)
			break;

	}
	if(i != 0)
		Debug::Log("Tick", "updated " + FString::FromInt(i) + " chunks, took " +
			Debug::Float(std::chrono::duration<float, std::milli>(std::chrono::high_resolution_clock::now() - start).count()) + " ms", this);

}

void AGenerator::generateChunk(FChunkData* element)
{
	AChunk* chunk = nullptr;
	SpawnChunk({1, 1, 1}, chunk);
	chunk->Init(noise, element);
	chunk->GenerateMesh(*element->mesh_data);
	chunk->SetActorLocation(FVector(element->index) * scale);
	// chunk->SetActorScale3D(chunk->GetActorScale3D() * 0.99);
	// Debug::Log("Generator", "generated chunk " + element->index.ToString(), this);
	// GeneratorBackground::EnqueueTask({(void(*)(void*))GeneratorBackground::CheckBorder, (void*)chunk});
}

void AGenerator::Init()
{
	const auto seed = FVector2D(UKismetMathLibrary::RandomFloat(), UKismetMathLibrary::RandomFloat());
	noise = new SimplexNoise(noise_data);

	background_generator_chunk = new GeneratorBackground(this, 1000);
	background_thread_chunk = FRunnableThread::Create(background_generator_chunk, TEXT("Background Generator"));
}

int AGenerator::getLOD(float distance) const
{
	return getLOD(LOD, distance);
}

int AGenerator::getLOD(TMap<float, int> LOD_, float distance)
{
	for (auto& lod : LOD_)
	{
		if(lod.Key < distance)
			continue;
		return lod.Value;
	}
	return -1;
}

AChunk* AGenerator::FindChunk(const FIntPoint& Index)
{
	auto data = FindChunkData(Index);
	return data ? data->chunk : nullptr;
}

FChunkData* AGenerator::FindChunkData(const FIntPoint& Index)
{
	for(int i = 0; i < chunks.Num(); i++)
	{
		if(chunks[i]->index == Index)
			return chunks[i];
	}
	return nullptr;
}


